// 1. Ül: Alla laadida API'st tekst.

var refreshMessages = async function() {
	// Lihtsalt selleks, et ma tean, et funktsioon läks käima
	console.log("refreshMessages läks käima")
	// API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	// fetch teeb päringu serverisse (meie defineeritud adre)
	var request = await fetch(APIurl)
	// json() käsk vormindab meile data mugavaks JSONiks
	var json = await request.json()

	// Kuva serverist saadud info HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages
	while (sonumid.length > 0) { // kuniks sõnumeid on
		var sonum = sonumid.shift()
		// lisa HTMLi #jutt sisse sonum.message
		document.querySelector('#jutt').innerHTML +=
			"<p>"+ sonum.user + ": " + sonum.message + "</p>"
	}

	// scrolli kõige alla
	window.scrollTo(0,document.body.scrollHeight);
}

// Uuenda sõnumeid iga sekund
setInterval(refreshMessages, 1000) // 1000 on sekund


document.querySelector('form').onsubmit = function(event) {
	event.preventDefault()
	// Korjame kokku formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = "" // tee input tühjaks
	console.log(username, message)

	// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message" // See on serveri poolt antud URL
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({user: username, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}