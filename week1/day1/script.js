console.log("Hello World!")

document.body.innerHTML += "<p>tere!</p>"

document.querySelector("ol li").innerHTML = "Muudetud!"

document.querySelector("form").onsubmit = function(e) {
	e.preventDefault()
	var nimeInput = document.querySelector("#nimi")

	if (nimeInput.value == "Krister") {
		document.body.innerHTML += 
			"<p>Tere, " + nimeInput.value + "</p>"
	} else {
		document.body.innerHTML += 
			"<p>Kes sa oled üldse, " + nimeInput.value + "</p>"
	}
}